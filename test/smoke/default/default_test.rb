# # encoding: utf-8

# Inspec test for recipe ecmc_redis::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe file('/etc/profile.d/Z99redis_environment.sh') do
  its('content') { should match(/redis/)}
  its('content') { should match(/REDIS_URL/)}
end

describe host('redis') do
  it { should be_resolvable }
  its('ipaddress') { should include '127.0.0.1' }
end

describe docker.containers do
  its('images') { should include 'redis:latest' }
  its('names') { should include 'ecmc-redis' }
end
