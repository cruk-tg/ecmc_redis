#
# Cookbook:: ecmc_redis
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

docker_image 'redis' do
  action :pull
end

if node.attribute?('development')
  docker_container 'ecmc-redis' do
    repo 'redis'
    port '6379:6379'
    restart_policy 'always'
    action :run
  end

  hostsfile_entry '127.0.0.1' do
    hostname 'redis'
    action :append
  end
else
  docker_container 'ecmc-redis' do
    repo 'redis'
    restart_policy 'always'
    action :run
  end
end

template '/etc/profile.d/Z99redis_environment.sh' do
  source 'etc/profile.d/Z99redis_environment.sh.erb'
  mode '0755'
  owner 'root'
  group 'root'
end
