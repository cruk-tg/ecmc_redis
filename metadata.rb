name 'ecmc_redis'
maintainer 'Paul Mitchell'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license 'All Rights Reserved'
description 'Installs/Configures Redis for ECMC use'
long_description 'Installs/Configures Redis for ECMC use'
version '0.3.0'
chef_version '>= 12.1' if respond_to?(:chef_version)

depends 'apt'
depends 'hostsfile'
depends 'docker', '~> 2.0'

supports 'ubuntu'
